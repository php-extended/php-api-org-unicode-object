<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-org-unicode-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiOrgUnicode;

/**
 * ApiOrgUnicodeDerivedNameRange class file.
 * 
 * This class represents a derived name range for the unicode codepoints.
 * 
 * @author Anastaszor
 */
class ApiOrgUnicodeRange implements ApiOrgUnicodeRangeInterface
{
	
	/**
	 * The hexa value of the first codepoint of the range.
	 * 
	 * @var string
	 */
	protected string $_firstCodepoint;
	
	/**
	 * The hexa value of the last codepoint of the range.
	 * 
	 * @var string
	 */
	protected string $_lastCodepoint;
	
	/**
	 * The name pattern for this range.
	 * 
	 * @var string
	 */
	protected string $_namePattern;
	
	/**
	 * Used for iterator behavior.
	 * 
	 * @var integer
	 */
	protected int $_currentCodepoint;
	
	/**
	 * Builds a new ApiOrgUnicodeDerivedNameRange with the given start, end
	 * codepoints and name pattern.
	 * 
	 * @param string $first
	 * @param string $last
	 * @param string $name
	 */
	public function __construct(string $first, string $last, string $name)
	{
		$this->_firstCodepoint = $first;
		$this->_lastCodepoint = $last;
		$this->_currentCodepoint = (int) \hexdec($this->_firstCodepoint);
		$this->_namePattern = $name;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return 'Range '.$this->_firstCodepoint.'-'.$this->_lastCodepoint;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::current()
	 */
	public function current() : ApiOrgUnicodeNameInterface
	{
		return new ApiOrgUnicodeName(
			\dechex($this->_currentCodepoint),
			\str_replace('*', (string) \mb_strtoupper(\dechex($this->_currentCodepoint)), $this->_namePattern),
		);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::next()
	 */
	public function next() : void
	{
		$this->_currentCodepoint++;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::key()
	 */
	public function key() : int
	{
		return $this->_currentCodepoint - (int) \hexdec($this->_firstCodepoint);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::valid()
	 */
	public function valid() : bool
	{
		return \hexdec($this->_lastCodepoint) >= $this->_currentCodepoint;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::rewind()
	 */
	public function rewind() : void
	{
		$this->_currentCodepoint = (int) \hexdec($this->_firstCodepoint);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiOrgUnicode\ApiOrgUnicodeRangeInterface::isBefore()
	 */
	public function isBefore(?string $codepoint) : bool
	{
		if(null === $codepoint)
		{
			return false;
		}
		
		$intcodepoint = \hexdec($codepoint);
		
		return \hexdec($this->_lastCodepoint) < $intcodepoint;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiOrgUnicode\ApiOrgUnicodeRangeInterface::isAfter()
	 */
	public function isAfter(?string $codepoint) : bool
	{
		if(null === $codepoint)
		{
			return false;
		}
		
		$intcodepoint = \hexdec($codepoint);
		
		return \hexdec($this->_firstCodepoint) > $intcodepoint;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiOrgUnicode\ApiOrgUnicodeRangeInterface::containsCodepoint()
	 */
	public function containsCodepoint(?string $codepoint) : bool
	{
		if(null === $codepoint)
		{
			return false;
		}
		
		$intcodepoint = \hexdec($codepoint);
		
		return \hexdec($this->_firstCodepoint) <= $intcodepoint
			&& \hexdec($this->_lastCodepoint) >= $intcodepoint;
	}
	
}
