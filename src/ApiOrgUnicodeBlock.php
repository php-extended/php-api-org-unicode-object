<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-org-unicode-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiOrgUnicode;

/**
 * ApiOrgUnicodeBlock class file.
 * 
 * This class represents a block for the unicode codepoints.
 * 
 * @author Anastaszor
 */
class ApiOrgUnicodeBlock implements ApiOrgUnicodeBlockInterface
{
	
	/**
	 * The hexa value of the first codepoint of the range.
	 * 
	 * @var string
	 */
	protected string $_firstCodepoint;
	
	/**
	 * The hexa value of the last codepoint of the range.
	 * 
	 * @var string
	 */
	protected string $_lastCodepoint;
	
	/**
	 * The name of the block.
	 * 
	 * @var string
	 */
	protected string $_name;
	
	/**
	 * Builds a new ApiOrgUnicodeBlock with the given start, end codepoints and
	 * name.
	 * 
	 * @param string $first
	 * @param string $last
	 * @param string $name
	 */
	public function __construct(string $first, string $last, string $name)
	{
		$this->_firstCodepoint = $first;
		$this->_lastCodepoint = $last;
		$this->_name = $name;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return 'Block '.$this->_firstCodepoint.'-'.$this->_lastCodepoint;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiOrgUnicode\ApiOrgUnicodeBlockInterface::getFirstCodepoint()
	 */
	public function getFirstCodepoint() : string
	{
		return $this->_firstCodepoint;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiOrgUnicode\ApiOrgUnicodeBlockInterface::getLastCodepoint()
	 */
	public function getLastCodepoint() : string
	{
		return $this->_lastCodepoint;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiOrgUnicode\ApiOrgUnicodeBlockInterface::getName()
	 */
	public function getName() : string
	{
		return $this->_name;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiOrgUnicode\ApiOrgUnicodeBlockInterface::containsCodepoint()
	 */
	public function containsCodepoint(?string $codepoint) : bool
	{
		if(null === $codepoint)
		{
			return false;
		}
		
		$intcodepoint = \hexdec($codepoint);
		
		return \hexdec($this->_firstCodepoint) <= $intcodepoint
			&& \hexdec($this->_lastCodepoint) >= $intcodepoint;
	}
	
}
