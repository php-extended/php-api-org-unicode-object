<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-org-unicode-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiOrgUnicode;

/**
 * ApiOrgUnicodeEntry class file.
 * 
 * This class represents a folder entry when listing available files.
 * 
 * @author Anastaszor
 */
class ApiOrgUnicodeEntry implements ApiOrgUnicodeEntryInterface
{
	
	/**
	 * The file type of the entry.
	 * 
	 * @var string
	 */
	protected string $_fileType;
	
	/**
	 * The path of the entry.
	 * 
	 * @var string
	 */
	protected string $_path;
	
	/**
	 * The name of the entry.
	 * 
	 * @var string
	 */
	protected string $_name;
	
	/**
	 * Builds a new entry with the given file type, path and name.
	 * 
	 * @param string $fileType
	 * @param string $path
	 * @param string $name
	 */
	public function __construct(string $fileType, string $path, string $name)
	{
		$this->_fileType = $fileType;
		$this->_path = $path;
		$this->_name = $name;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.$this->_path;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiOrgUnicode\ApiOrgUnicodeEntryInterface::isFile()
	 */
	public function isFile() : bool
	{
		return !$this->isFolder();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiOrgUnicode\ApiOrgUnicodeEntryInterface::isFolder()
	 */
	public function isFolder() : bool
	{
		return 'DIR' === $this->_fileType;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiOrgUnicode\ApiOrgUnicodeEntryInterface::getFileType()
	 */
	public function getFileType() : string
	{
		return $this->_fileType;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiOrgUnicode\ApiOrgUnicodeEntryInterface::getPath()
	 */
	public function getPath() : string
	{
		return $this->_path;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiOrgUnicode\ApiOrgUnicodeEntryInterface::getName()
	 */
	public function getName() : string
	{
		return $this->_name;
	}
	
}
