<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-org-unicode-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiOrgUnicode;

/**
 * ApiOrgUnicodeCodepoint class file.
 * 
 * This class represents a derived name for a given codepoint.
 * 
 * @author Anastaszor
 */
class ApiOrgUnicodeCodepoint implements ApiOrgUnicodeCodepointInterface
{
	
	/**
	 * The hexa value of the codepoint.
	 * 
	 * @var string
	 */
	protected string $_codepoint;
	
	/**
	 * The name of the codepoint.
	 * 
	 * @var string
	 */
	protected string $_name;
	
	/**
	 * The block of the codepoint.
	 * 
	 * @var ApiOrgUnicodeBlockInterface
	 */
	protected ApiOrgUnicodeBlockInterface $_block;
	
	/**
	 * Builds a new ApiOrgUnicodeCodepoint with the given codepoint and name.
	 * 
	 * @param string $codepoint
	 * @param string $name
	 * @param ApiOrgUnicodeBlockInterface $block
	 */
	public function __construct(string $codepoint, string $name, ApiOrgUnicodeBlockInterface $block)
	{
		$this->_codepoint = $codepoint;
		$this->_name = $name;
		$this->_block = $block;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiOrgUnicode\ApiOrgUnicodeCodepointInterface::getCodepoint()
	 */
	public function getCodepoint() : string
	{
		return $this->_codepoint;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiOrgUnicode\ApiOrgUnicodeCodepointInterface::getUnicodeCodepoint()
	 */
	public function getUnicodeCodepoint() : string
	{
		return 'U+'.$this->getCodepoint();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiOrgUnicode\ApiOrgUnicodeCodepointInterface::getJsonCodepoint()
	 */
	public function getJsonCodepoint() : string
	{
		return '\\u'.$this->getCodepoint();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiOrgUnicode\ApiOrgUnicodeCodepointInterface::getPhpCodepoint()
	 */
	public function getPhpCodepoint() : string
	{
		return '\\u{'.$this->getCodepoint().'}';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiOrgUnicode\ApiOrgUnicodeCodepointInterface::getUtf8Chain()
	 */
	public function getUtf8Chain() : string
	{
		$cpt = [];
		
		foreach((array) \mb_str_split($this->getCodepoint(), 2) as $cpstr)
		{
			$cpt[] = '\\x'.((string) $cpstr);
		}
		
		return \implode('', $cpt);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiOrgUnicode\ApiOrgUnicodeCodepointInterface::getUtf8String()
	 */
	public function getUtf8String() : string
	{
		$num = (int) \hexdec($this->_codepoint);
		
		if(0x7F >= $num)
		{
			return \chr($num);
		}
		
		if(0x7FF >= $num)
		{
			return \chr(($num >> 6) + 192)
				  .\chr(($num & 63) + 128);
		}
		
		if(0xFFFF >= $num)
		{
			return \chr(($num >> 12) + 224)
				  .\chr((($num >> 6) & 63) + 128)
				  .\chr(($num & 63) + 128);
		}
		
		// if(0x1FFFFF >= $num)
		return \chr(($num >> 18) + 240)
			  .\chr((($num >> 12) & 63) + 128)
			  .\chr((($num >> 6) & 63) + 128)
			  .\chr(($num & 63) + 128);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiOrgUnicode\ApiOrgUnicodeCodepointInterface::getName()
	 */
	public function getName() : string
	{
		return $this->_name;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiOrgUnicode\ApiOrgUnicodeCodepointInterface::getBlock()
	 */
	public function getBlock() : ApiOrgUnicodeBlockInterface
	{
		return $this->_block;
	}
	
}
