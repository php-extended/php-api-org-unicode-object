<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-org-unicode-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiOrgUnicode;

use ArrayIterator;
use Iterator;
use Stringable;

/**
 * ApiOrgUnicodeCodepointIterator class file.
 * 
 * This class represents a builder for codepoints by aggregating disseminated
 * data amongst unicode consortium data files.
 * 
 * @author Anastaszor
 * @extends \ArrayIterator<integer, ApiOrgUnicodeCodepointInterface>
 */
class ApiOrgUnicodeCodepointIterator extends ArrayIterator implements Stringable
{
	
	/**
	 * Hardcoded character names that are not present in the derived names file.
	 * 
	 * @var array<string, array<string, string>>
	 */
	protected array $_hardCharacterNames = [
		'0000' => [
			'0000' => 'NULL',
			'0001' => 'START OF HEADING',
			'0002' => 'START OF TEXT',
			'0003' => 'END OF TEXT',
			'0004' => 'END OF TRANSMISSION',
			'0005' => 'ENQUIRY',
			'0006' => 'ACKNOWLEDGE',
			'0007' => 'BELL',
			'0008' => 'BACKSPACE',
			'0009' => 'HORIZONTAL TABULATION',
			'000A' => 'NEW LINE',
			'000B' => 'VERTICAL TABULATION',
			'000C' => 'FORM FEED',
			'000D' => 'CARRIAGE RETURN',
			'000E' => 'SHIFT OUT',
			'000F' => 'SHIFT IN',
			'0010' => 'DATA LINK ESCAPE',
			'0011' => 'DEVICE CONTROL ONE',
			'0012' => 'DEVICE CONTROL TWO',
			'0013' => 'DEVICE CONTROL THREE',
			'0014' => 'DEVICE CONTROL FOUR',
			'0015' => 'NEGATIVE ACKNOWLEDGE',
			'0016' => 'SYNCHRONOUS IDLE',
			'0017' => 'END OF TRANSMISSION BLOCK',
			'0018' => 'CANCEL',
			'0019' => 'END OF MEDIUM',
			'001A' => 'SUBSTITUTE',
			'001B' => 'ESCAPE',
			'001C' => 'FILE SEPARATOR',
			'001D' => 'GROUP SEPARATOR',
			'001E' => 'RECORD SEPARATOR',
			'001F' => 'UNIT SEPARATOR',
			'007F' => 'DELETE',
		],
		'0080' => [
			'0080' => 'PADDING CHARACTER',
			'0081' => 'HIGH OCTET PRESENT',
			'0082' => 'BREAK PERMITTED HERE',
			'0083' => 'NO BREAK HERE',
			'0084' => 'INDEX',
			'0085' => 'NEXT LINE',
			'0086' => 'START OF SELECTED AREA',
			'0087' => 'END OF SELECTED AREA',
			'0088' => 'CHARACTER TABULATION SET',
			'0089' => 'CHARACTER TABULATION WITH JUSTIFICATION',
			'008A' => 'LINE TABULATION SET',
			'008B' => 'PARTIAL LINE FORWARD',
			'008C' => 'PARTIAL LINE BACKWARD',
			'008D' => 'REVERSE LINE FEED',
			'008E' => 'SINGLE SHIFT TWO',
			'008F' => 'SINGLE SHIFT THREE',
			'0090' => 'DEVICE CONTROL STRING',
			'0091' => 'PRIVATE USE ONE',
			'0092' => 'PRIVATE USE TWO',
			'0093' => 'SET TRANSMIT STATE',
			'0094' => 'CANCEL CHARACTER',
			'0095' => 'MESSAGE WAITING',
			'0096' => 'START OF GUARDED AREA',
			'0097' => 'END OF GUARDED AREA',
			'0098' => 'START OF STRING',
			'0099' => 'SINGLE GRAPHIC CHARACTER INTRODUCER',
			'009A' => 'SINGLE CHARACTER INTRODUCER',
			'009B' => 'CONTROL SEQUENCE INTRODUCER',
			'009C' => 'STRING TERMINATOR',
			'009D' => 'OPERATING SYSTEM COMMAND',
			'009E' => 'PRIVACY MESSAGE',
			'009F' => 'APPLICATION PROGRAM COMMAND',
		],
	];
	
	/**
	 * Builds a new ApiOrgUnicodeCodepointIterator class file with the given
	 * auxiliary iterators.
	 * 
	 * @param Iterator<ApiOrgUnicodeBlockInterface> $blockIterator
	 * @param Iterator<ApiOrgUnicodeRangeInterface> $derivedNamesIterator
	 */
	public function __construct(
		Iterator $blockIterator,
		Iterator $derivedNamesIterator
	) {
		$codepoints = [];
		$blocks = [];
		
		/** @var ApiOrgUnicodeBlockInterface $block */
		foreach($blockIterator as $block)
		{
			$blocks[] = $block;
			
			if(isset($this->_hardCharacterNames[$block->getFirstCodepoint()]))
			{
				foreach($this->_hardCharacterNames[$block->getFirstCodepoint()] as $codepoint => $name)
				{
					$codepoints[] = new ApiOrgUnicodeCodepoint($codepoint, $name, $block);
				}
			}
		}
		
		/** @var ApiOrgUnicodeRangeInterface $range */
		foreach($derivedNamesIterator as $range)
		{
			/** @var ApiOrgUnicodeNameInterface $value */
			foreach($range as $value)
			{
				$block = null;
				
				/** @var ApiOrgUnicodeBlockInterface $curBlock */
				foreach($blocks as $curBlock)
				{
					if($curBlock->containsCodepoint($value->getCodepoint()))
					{
						$block = $curBlock;
						break;
					}
				}
				
				if(null === $block)
				{
					continue;
				}
				
				$codepoints[] = new ApiOrgUnicodeCodepoint($value->getCodepoint(), $value->getName(), $block);
			}
		}
		
		parent::__construct($codepoints);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
}
