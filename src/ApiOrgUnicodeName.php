<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-org-unicode-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiOrgUnicode;

/**
 * ApiOrgUnicodeName class file.
 * 
 * This class represents a derived name for a given codepoint.
 * 
 * @author Anastaszor
 */
class ApiOrgUnicodeName implements ApiOrgUnicodeNameInterface
{
	
	/**
	 * The hexa value of the codepoint.
	 * 
	 * @var string
	 */
	protected string $_codepoint;
	
	/**
	 * The name of the codepoint.
	 * 
	 * @var string
	 */
	protected string $_name;
	
	/**
	 * Builds a new ApiOrgUnicodeDerivedName with the given codepoint and name.
	 * 
	 * @param string $codepoint
	 * @param string $name
	 */
	public function __construct(string $codepoint, string $name)
	{
		$this->_codepoint = \str_pad((string) \mb_strtoupper($codepoint), 4, '0', \STR_PAD_LEFT);
		$this->_name = $name;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return $this->_codepoint.' '.$this->_name;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiOrgUnicode\ApiOrgUnicodeNameInterface::getCodepoint()
	 */
	public function getCodepoint() : string
	{
		return $this->_codepoint;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiOrgUnicode\ApiOrgUnicodeNameInterface::getName()
	 */
	public function getName() : string
	{
		return $this->_name;
	}
	
}
