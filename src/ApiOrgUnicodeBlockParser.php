<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-org-unicode-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiOrgUnicode;

use ArrayIterator;
use Iterator;
use PhpExtended\Parser\AbstractParser;
use PhpExtended\Parser\ParseException;

/**
 * ApiOrgUnicodeBlockParser class file.
 * 
 * This class transforms block data into a list of block objects.
 * 
 * @author Anastaszor
 * @extends AbstractParser<Iterator<ApiOrgUnicodeBlockInterface>>
 */
class ApiOrgUnicodeBlockParser extends AbstractParser
{
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Parser\ParserInterface::parse()
	 * @return Iterator<ApiOrgUnicodeBlockInterface>
	 */
	public function parse(?string $data) : Iterator
	{
		$data = (string) $data;
		if('' === $data)
		{
			$message = 'Faile to parse block from an empty string.';
			
			throw new ParseException(ApiOrgUnicodeBlockInterface::class, $data, 0, $message);
		}
		
		if(false === \mb_strpos($data, '# Blocks-'))
		{
			$message = 'The data should be the txt file that lists blocks.';
			
			throw new ParseException(ApiOrgUnicodeBlockInterface::class, $data, 0, $message);
		}
		
		$arrdata = \explode("\n", $data);
		if(2 > \count($arrdata))
		{
			$message = 'The given data does not represents a list of ranges.';
			
			throw new ParseException(ApiOrgUnicodeBlockInterface::class, $data, 0, $message);
		}
		
		$iterator = new ArrayIterator();
		$offset = 0;
		
		foreach($arrdata as $linenb => $contents)
		{
			$offset += (1 + (int) \mb_strlen($contents));
			
			// ignore empty lines
			if(empty($contents))
			{
				continue;
			}
			
			// ignore commented lines
			if('#' === $contents[0])
			{
				continue;
			}
			
			// all other lines are data lines
			$matches = [];
			if(\preg_match('#^([A-F0-9]{4,6})\\.\\.([A-F0-9]{4,6}); ([A-Za-z0-9 -]+)$#', $contents, $matches))
			{
				/** @phpstan-ignore-next-line */ /** @psalm-suppress InvalidArgument */
				$iterator->append(new ApiOrgUnicodeBlock($matches[1] ?? '0000', $matches[2] ?? '0000', $matches[3] ?? 'FAIL'));
				
				continue;
			}
			
			$message = 'Failed to parse line {line} with contents "{contents}"';
			$context = ['{line}' => $linenb, '{contents}' => $contents];
			
			throw new ParseException(ApiOrgUnicodeBlockInterface::class, $data, $offset, \strtr($message, $context));
		}
		
		return $iterator;
	}
	
}
