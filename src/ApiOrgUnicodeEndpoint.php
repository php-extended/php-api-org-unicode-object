<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-org-unicode-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiOrgUnicode;

use InvalidArgumentException;
use Iterator;
use PhpExtended\Html\HtmlParser;
use PhpExtended\Html\HtmlParserInterface;
use PhpExtended\HttpMessage\RequestFactory;
use PhpExtended\HttpMessage\UriFactory;
use PhpExtended\Parser\ParseThrowable;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\UriFactoryInterface;
use Throwable;

/**
 * ApiOrgUnicodeEndpoint class file.
 * 
 * This class represents the www.unicode.org website and has all the methods
 * to gather data files to interpret them.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 */
class ApiOrgUnicodeEndpoint implements ApiOrgUnicodeEndpointInterface
{
	
	/**
	 * The hostname of the unicode website.
	 * 
	 * @var string
	 */
	public const HOST = 'https://www.unicode.org/';
	
	/**
	 * The client.
	 * 
	 * @var ClientInterface
	 */
	protected ClientInterface $_httpClient;
	
	/**
	 * The uri factory.
	 * 
	 * @var UriFactoryInterface
	 */
	protected UriFactoryInterface $_uriFactory;
	
	/**
	 * The request factory.
	 * 
	 * @var RequestFactoryInterface
	 */
	protected RequestFactoryInterface $_requestFactory;
	
	/**
	 * The html parser.
	 * 
	 * @var HtmlParserInterface
	 */
	protected HtmlParserInterface $_htmlParser;
	
	/**
	 * All the available versions of the unicode database, ordered by.
	 * 
	 * @var array<integer, string>
	 */
	protected array $_versions = [];
	
	/**
	 * Builds a new ApiOrgUnicodeEndpoint.
	 * 
	 * @param ClientInterface $client
	 * @param ?UriFactoryInterface $uriFactory
	 * @param ?RequestFactoryInterface $requestFactory
	 * @param ?HtmlParserInterface $htmlParser
	 */
	public function __construct(
		ClientInterface $client,
		?UriFactoryInterface $uriFactory = null,
		?RequestFactoryInterface $requestFactory = null,
		?HtmlParserInterface $htmlParser = null
	) {
		$this->_httpClient = $client;
		$this->_uriFactory = $uriFactory ?? new UriFactory();
		$this->_requestFactory = $requestFactory ?? new RequestFactory();
		$this->_htmlParser = $htmlParser ?? new HtmlParser();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiOrgUnicode\ApiOrgUnicodeEndpointInterface::getVersions()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException
	 * @throws ParseThrowable
	 */
	public function getVersions() : array
	{
		if([] === $this->_versions)
		{
			$uri = $this->_uriFactory->createUri(self::HOST.'Public/');
			$request = $this->_requestFactory->createRequest('GET', $uri);
			$response = $this->_httpClient->sendRequest($request);
			$dom = $this->_htmlParser->parse($response->getBody()->__toString());
			
			foreach($dom->findAllNodesCss('a') as $anchor)
			{
				$href = $anchor->getAttribute('href');
				if(null === $href)
				{
					// @codeCoverageIgnoreStart
					continue; // should not happen
					// @codeCoverageIgnoreEnd
				}
				$hrefval = $href->getValue();
				
				$matches = [];
				if(!\preg_match('#^(\\d+)\\.(\\d+)[.-].+?(\\d+)?/$#', $hrefval, $matches))
				{
					continue;
				}
				
				$version = \trim($hrefval, '/');
				
				try
				{
					$uri = $this->_uriFactory->createUri(self::HOST.'Public/zipped/'.$version.'/ReadMe.txt');
					$request = $this->_requestFactory->createRequest('GET', $uri);
					$response = $this->_httpClient->sendRequest($request);
					if($response->getStatusCode() === 200)
					{
						$this->_versions[] = $version;
					}
				}
				catch(Throwable $exc)
				{
					// if the readme file does not exist, ignore the version
				}
			}
		}
		
		return $this->_versions;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiOrgUnicode\ApiOrgUnicodeEndpointInterface::getLatestVersion()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException
	 * @throws ParseThrowable
	 */
	public function getLatestVersion() : string
	{
		if([] === $this->_versions)
		{
			$this->getVersions();
		}
		
		return $this->_versions[\count($this->_versions) - 1];
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiOrgUnicode\ApiOrgUnicodeEndpointInterface::getBlocks()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException
	 * @throws ParseThrowable
	 */
	public function getBlocks(?string $version = null) : Iterator
	{
		$version = $this->ensureVersion($version);
		
		$uri = $this->_uriFactory->createUri(self::HOST.'Public/'.$version.'/ucd/Blocks.txt');
		$request = $this->_requestFactory->createRequest('GET', $uri);
		$response = $this->_httpClient->sendRequest($request);
		
		$blockParser = new ApiOrgUnicodeBlockParser();
		
		return $blockParser->parse($response->getBody()->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiOrgUnicode\ApiOrgUnicodeEndpointInterface::getCodepoints()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException
	 * @throws ParseThrowable
	 */
	public function getCodepoints(?string $version = null) : Iterator
	{
		$version = $this->ensureVersion($version);
		
		$blockIterator = $this->getBlocks($version);
		$derivedNameRangesIterator = $this->getDerivedNameRanges($version);
		
		return new ApiOrgUnicodeCodepointIterator(
			$blockIterator,
			$derivedNameRangesIterator,
		);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiOrgUnicode\ApiOrgUnicodeEndpointInterface::getDerivedNameRanges()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException
	 * @throws ParseThrowable
	 */
	public function getDerivedNameRanges(?string $version = null) : Iterator
	{
		$version = $this->ensureVersion($version);
		
		$uri = $this->_uriFactory->createUri(self::HOST.'Public/'.$version.'/ucd/extracted/DerivedName.txt');
		$request = $this->_requestFactory->createRequest('GET', $uri);
		$response = $this->_httpClient->sendRequest($request);
		
		$derivedNamesParser = new ApiOrgUnicodeRangeParser();
		
		return $derivedNamesParser->parse($response->getBody()->__toString());
	}
	
	/**
	 * Ensures that the version is a valid one.
	 * 
	 * @param ?string $version
	 * @return string
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException
	 * @throws ParseThrowable
	 */
	protected function ensureVersion(?string $version) : string
	{
		$version = (string) $version;
		if('' === $version)
		{
			$message = 'Failed to endure version from empty string.';
			
			throw new InvalidArgumentException($message);
		}
		
		if(!\in_array($version, $this->getVersions(), true))
		{
			$message = 'Failed to find version number "{value}" in "{list}"';
			$context = ['{value}' => $version, '{list}' => \implode(', ', $this->getVersions())];
			
			throw new InvalidArgumentException(\strtr($message, $context));
		}
		
		return $version;
	}
	
}
