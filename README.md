# php-extended/php-api-org-unicode-object

A php API wrapper to interpret data files of the unicode consortium at www.unicode.org.

![coverage](https://gitlab.com/php-extended/php-api-org-unicode-object/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-api-org-unicode-object/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-api-org-unicode-object ^8`


## Basic Usage

You may use this library the following way :

```php

use PhpExtended\UnicodeOrgApi\UnicodeOrgApiEndpoint;
use PhpExtended\Endpoint\HttpEndpoint;

/* @var $client \Psr\Http\Client\ClientInterface */

$endpoint = new UnicodeOrgApiEndpoint(new HttpEndpoint($http));

$blocks = $endpoint->getBlocks();	// returns \Iterator<UnicodeOrgApiBlock>

```


## License

MIT (See [license file](LICENSE)).
