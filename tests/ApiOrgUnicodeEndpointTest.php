<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-org-unicode-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\ApiOrgUnicode\ApiOrgUnicodeBlockInterface;
use PhpExtended\ApiOrgUnicode\ApiOrgUnicodeCodepointInterface;
use PhpExtended\ApiOrgUnicode\ApiOrgUnicodeEndpoint;
use PhpExtended\ApiOrgUnicode\ApiOrgUnicodeRangeInterface;
use PhpExtended\HttpMessage\Response;
use PhpExtended\HttpMessage\StringStream;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * ApiOrgUnicodeEndpointTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiOrgUnicode\ApiOrgUnicodeEndpoint
 *
 * @internal
 *
 * @small
 */
class ApiOrgUnicodeEndpointTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiOrgUnicodeEndpoint
	 */
	protected ApiOrgUnicodeEndpoint $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetVersions() : void
	{
		$this->assertNotEmpty($this->_object->getVersions());
	}
	
	public function testGetLastVersion() : void
	{
		// \var_dump($this->_object->getVersions());
		$this->assertEquals('16.0.0', $this->_object->getLatestVersion());
	}
	
	public function testGetBlocks() : void
	{
		$k = 0;
		
		foreach($this->_object->getBlocks($this->_object->getLatestVersion()) as $key => $block)
		{
			$this->assertIsInt($key);
			$this->assertInstanceOf(ApiOrgUnicodeBlockInterface::class, $block);
			$k++;
		}
		
		$this->assertGreaterThan(1, $k);
	}
	
	public function testGetCodePoints() : void
	{
		$k = 0;
		
		foreach($this->_object->getCodepoints($this->_object->getLatestVersion()) as $key => $codepoint)
		{
			$this->assertIsInt($key);
			$this->assertInstanceOf(ApiOrgUnicodeCodepointInterface::class, $codepoint);
			$k++;
		}
		
		$this->assertGreaterThan(1, $k);
	}
	
	public function testGetDerivedNameRanges() : void
	{
		$k = 0;
		
		foreach($this->_object->getDerivedNameRanges($this->_object->getLatestVersion()) as $key => $range)
		{
			$this->assertIsInt($key);
			$this->assertInstanceOf(ApiOrgUnicodeRangeInterface::class, $range);
			$k++;
		}
		
		$this->assertGreaterThan(1, $k);
	}
	
	public function testNullVersion() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_object->getBlocks(null);
	}
	
	public function testImpossibleVersion() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_object->getBlocks('v42.67.98.AZERTYUIOP');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$client = new class() implements ClientInterface
		{
			
			public function sendRequest(RequestInterface $request) : ResponseInterface
			{
				$options = ['http' => ['user_agent' => 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0']];
				// \var_dump($request->getUri()->__toString());
				$data = @\file_get_contents($request->getUri()->__toString(), false, \stream_context_create($options));
				if(false === $data)
				{
					throw new RuntimeException('Failed to get data from '.$request->getUri()->__toString());
				}
				
				return (new Response())->withBody(new StringStream($data));
			}
		};
		
		$this->_object = new ApiOrgUnicodeEndpoint($client);
	}
	
}
