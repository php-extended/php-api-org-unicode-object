<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-org-unicode-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\ApiOrgUnicode\ApiOrgUnicodeBlock;
use PhpExtended\ApiOrgUnicode\ApiOrgUnicodeCodepoint;
use PHPUnit\Framework\TestCase;

/**
 * ApiOrgUnicodeCodepointTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiOrgUnicode\ApiOrgUnicodeCodepoint
 *
 * @internal
 *
 * @small
 */
class ApiOrgUnicodeCodepointTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiOrgUnicodeCodepoint
	 */
	protected ApiOrgUnicodeCodepoint $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetCodepoint() : void
	{
		$this->assertEquals('0123', $this->_object->getCodepoint());
	}
	
	public function testGetUnicodeCodepoint() : void
	{
		$this->assertEquals('U+0123', $this->_object->getUnicodeCodepoint());
	}
	
	public function testGetJsonCodepoint() : void
	{
		$this->assertEquals('\\u0123', $this->_object->getJsonCodepoint());
	}
	
	public function testGetPhpCodepoint() : void
	{
		$this->assertEquals('\\u{0123}', $this->_object->getPhpCodepoint());
	}
	
	public function testGetUtf8Chain() : void
	{
		$this->assertEquals('\\x01\\x23', $this->_object->getUtf8Chain());
	}
	
	public function testGetUtf8String() : void
	{
		$this->assertEquals("\xC4\xA3", $this->_object->getUtf8String());
	}
	
	public function testGetUtf8Simple() : void
	{
		$this->assertEquals("\x41", (new ApiOrgUnicodeCodepoint('0041', 'A', new ApiOrgUnicodeBlock('0000', 'FFFF', 'Block')))->getUtf8String());
	}
	
	public function testGetUtf8Trio() : void
	{
		$this->assertEquals("\xE2\x99\xA5", (new ApiOrgUnicodeCodepoint('2665', '♥ Black Heart Suit Emoji', new ApiOrgUnicodeBlock('0000', 'FFFF', 'Block')))->getUtf8String());
	}
	
	public function testGetUtf8Quatro() : void
	{
		$this->assertEquals("\xF3\xA0\x80\x81", (new ApiOrgUnicodeCodepoint('E0001', 'Language Tag', new ApiOrgUnicodeBlock('0000', 'FFFF', 'Block')))->getUtf8String());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('This Codepoint', $this->_object->getName());
	}
	
	public function testGetBlock() : void
	{
		$expected = new ApiOrgUnicodeBlock('0000', 'FFFF', 'This Block');
		
		$this->assertEquals($expected, $this->_object->getBlock());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiOrgUnicodeCodepoint(
			'0123',
			'This Codepoint',
			new ApiOrgUnicodeBlock('0000', 'FFFF', 'This Block'),
		);
	}
	
}
