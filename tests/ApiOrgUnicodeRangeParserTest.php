<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-org-unicode-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\ApiOrgUnicode\ApiOrgUnicodeRangeInterface;
use PhpExtended\ApiOrgUnicode\ApiOrgUnicodeRangeParser;
use PhpExtended\Parser\ParseException;
use PHPUnit\Framework\TestCase;

/**
 * ApiOrgUnicodeRangeParserTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiOrgUnicode\ApiOrgUnicodeRangeParser
 *
 * @internal
 *
 * @small
 */
class ApiOrgUnicodeRangeParserTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiOrgUnicodeRangeParser
	 */
	protected ApiOrgUnicodeRangeParser $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testParseEmpty() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse(null);
	}
	
	public function testParseFakeData() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('this is some fake data');
	}
	
	public function testParseOneliner() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('# DerivedName-range');
	}
	
	public function testParseFailed() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse(
			<<<EOF
# DerivedName-Range
this is random data
EOF,
		);
	}
	
	public function testParseSuccess() : void
	{
		$k = 0;
		
		foreach($this->_object->parse(
			<<<EOF
# DerivedName-Range

# comment

0000 ; An unique codepoint
0001..000F ; A range
EOF,
		) as $data)
		{
			$this->assertInstanceOf(ApiOrgUnicodeRangeInterface::class, $data);
			$k++;
		}
		
		$this->assertEquals(2, $k);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiOrgUnicodeRangeParser();
	}
	
}
