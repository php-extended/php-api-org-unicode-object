<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-org-unicode-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\ApiOrgUnicode\ApiOrgUnicodeBlock;
use PhpExtended\ApiOrgUnicode\ApiOrgUnicodeCodepointInterface;
use PhpExtended\ApiOrgUnicode\ApiOrgUnicodeCodepointIterator;
use PhpExtended\ApiOrgUnicode\ApiOrgUnicodeRange;
use PHPUnit\Framework\TestCase;

/**
 * ApiOrgUnicodeCodepointIteratorTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiOrgUnicode\ApiOrgUnicodeCodepointIterator
 *
 * @internal
 *
 * @small
 */
class ApiOrgUnicodeCodepointIteratorTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiOrgUnicodeCodepointIterator
	 */
	protected ApiOrgUnicodeCodepointIterator $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testItWorks() : void
	{
		$k = 0;
		
		foreach($this->_object as $codePoint)
		{
			$this->assertInstanceOf(ApiOrgUnicodeCodepointInterface::class, $codePoint);
			$k++;
		}
		
		$this->assertGreaterThan(1, $k);
	}
	
	public function testEmptyBlockCodepointIterator() : void
	{
		$k = 0;
		
		foreach((new ApiOrgUnicodeCodepointIterator(
			new ArrayIterator(),
			new ArrayIterator([
				new ApiOrgUnicodeRange('0000', '0000', 'First Range'),
			]),
		)
		) as $codePoint)
		{
			$this->assertInstanceOf(ApiOrgUnicodeCodepointInterface::class, $codePoint);
			$k++;
		}
		
		$this->assertEquals(0, $k);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiOrgUnicodeCodepointIterator(new ArrayIterator([
			new ApiOrgUnicodeBlock('0000', '0FFF', 'First'),
			new ApiOrgUnicodeBlock('1000', '1FFF', 'Secnd'),
		]), new ArrayIterator([
			new ApiOrgUnicodeRange('0000', '0000', 'First Range'),
			new ApiOrgUnicodeRange('0001', '0001', 'Second Range'),
		]));
	}
	
}
