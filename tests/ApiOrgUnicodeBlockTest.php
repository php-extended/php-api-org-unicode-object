<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-org-unicode-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\ApiOrgUnicode\ApiOrgUnicodeBlock;
use PHPUnit\Framework\TestCase;

/**
 * ApiOrgUnicodeBlockTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiOrgUnicode\ApiOrgUnicodeBlock
 *
 * @internal
 *
 * @small
 */
class ApiOrgUnicodeBlockTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiOrgUnicodeBlock
	 */
	protected ApiOrgUnicodeBlock $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('Block 0000-FFFF', $this->_object->__toString());
	}
	
	public function testGetFirstCodepoint() : void
	{
		$this->assertEquals('0000', $this->_object->getFirstCodepoint());
	}
	
	public function testGetLastCodepoint() : void
	{
		$this->assertEquals('FFFF', $this->_object->getLastCodepoint());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('This Block', $this->_object->getName());
	}
	
	public function testContainsCodepoint() : void
	{
		$this->assertTrue($this->_object->containsCodepoint('CCCC'));
	}
	
	public function testContainsCodepointFailed() : void
	{
		$this->assertFalse($this->_object->containsCodepoint(null));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiOrgUnicodeBlock('0000', 'FFFF', 'This Block');
	}
	
}
