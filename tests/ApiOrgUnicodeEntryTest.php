<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-org-unicode-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\ApiOrgUnicode\ApiOrgUnicodeEntry;
use PHPUnit\Framework\TestCase;

/**
 * ApiOrgUnicodeEntryTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiOrgUnicode\ApiOrgUnicodeEntry
 *
 * @internal
 *
 * @small
 */
class ApiOrgUnicodeEntryTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiOrgUnicodeEntry
	 */
	protected ApiOrgUnicodeEntry $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@/path/to/file', $this->_object->__toString());
	}
	
	public function testIsFile() : void
	{
		$this->assertTrue($this->_object->isFile());
	}
	
	public function testIsFolder() : void
	{
		$this->assertFalse($this->_object->isFolder());
	}
	
	public function testGetFileType() : void
	{
		$this->assertEquals('FILE', $this->_object->getFileType());
	}
	
	public function testGetPath() : void
	{
		$this->assertEquals('/path/to/file', $this->_object->getPath());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('name', $this->_object->getName());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiOrgUnicodeEntry('FILE', '/path/to/file', 'name');
	}
	
}
