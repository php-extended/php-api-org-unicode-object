<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-org-unicode-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\ApiOrgUnicode\ApiOrgUnicodeBlockInterface;
use PhpExtended\ApiOrgUnicode\ApiOrgUnicodeBlockParser;
use PhpExtended\Parser\ParseException;
use PHPUnit\Framework\TestCase;

/**
 * ApiOrgUnicodeBlockParserTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiOrgUnicode\ApiOrgUnicodeBlockParser
 *
 * @internal
 *
 * @small
 */
class ApiOrgUnicodeBlockParserTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiOrgUnicodeBlockParser
	 */
	protected ApiOrgUnicodeBlockParser $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testParseEmpty() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse(null);
	}
	
	public function testParseFakeData() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('this is some fake data');
	}
	
	public function testParseOneLiner() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('# Blocks-oneliner');
	}
	
	public function testParseFailed() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse(
			<<<EOF
# Blocks-list
this is random data
EOF,
		);
	}
	
	public function testParseSuccess() : void
	{
		$k = 0;
		
		foreach($this->_object->parse(
			<<<EOF
# Blocks-list

# comment

0000..007F; block 1
0080..00FF; block 2
EOF,
		) as $data)
		{
			$this->assertInstanceOf(ApiOrgUnicodeBlockInterface::class, $data);
			$k++;
		}
		
		$this->assertEquals(2, $k);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiOrgUnicodeBlockParser();
	}
	
}
