<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-org-unicode-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\ApiOrgUnicode\ApiOrgUnicodeNameInterface;
use PhpExtended\ApiOrgUnicode\ApiOrgUnicodeRange;
use PHPUnit\Framework\TestCase;

/**
 * ApiOrgUnicodeRangeTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiOrgUnicode\ApiOrgUnicodeRange
 *
 * @internal
 *
 * @small
 */
class ApiOrgUnicodeRangeTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiOrgUnicodeRange
	 */
	protected ApiOrgUnicodeRange $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('Range 1111-2222', $this->_object->__toString());
	}
	
	public function testIsBeforeFailed() : void
	{
		$this->assertFalse($this->_object->isBefore(null));
	}
	
	public function testIsBeforeTrue() : void
	{
		$this->assertTrue($this->_object->isBefore('2223'));
	}
	
	public function testIsBeforeFalse() : void
	{
		$this->assertFalse($this->_object->isBefore('2222'));
	}
	
	public function testIsAfterFailed() : void
	{
		$this->assertFalse($this->_object->isAfter(null));
	}
	
	public function testIsAfterTrue() : void
	{
		$this->assertTrue($this->_object->isAfter('1110'));
	}
	
	public function testIsAfterFalse() : void
	{
		$this->assertFalse($this->_object->isAfter('1111'));
	}
	
	public function testContainsCodepointFailed() : void
	{
		$this->assertFalse($this->_object->containsCodepoint(null));
	}
	
	public function testContainsCodepointTrue() : void
	{
		$this->assertTrue($this->_object->containsCodepoint('2000'));
	}
	
	public function testLoop() : void
	{
		$k = 0;
		
		foreach($this->_object as $key => $name)
		{
			$this->assertIsInt($key);
			$this->assertInstanceOf(ApiOrgUnicodeNameInterface::class, $name);
			$k++;
		}
		
		$this->assertEquals(0x1112, $k);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiOrgUnicodeRange('1111', '2222', 'This Range');
	}
	
}
