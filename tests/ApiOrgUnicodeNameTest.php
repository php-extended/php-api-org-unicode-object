<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-org-unicode-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\ApiOrgUnicode\ApiOrgUnicodeName;
use PHPUnit\Framework\TestCase;

/**
 * ApiOrgUnicodeNameTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiOrgUnicode\ApiOrgUnicodeName
 *
 * @internal
 *
 * @small
 */
class ApiOrgUnicodeNameTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiOrgUnicodeName
	 */
	protected ApiOrgUnicodeName $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('007F DEL', $this->_object->__toString());
	}
	
	public function testGetCodepoint() : void
	{
		$this->assertEquals('007F', $this->_object->getCodepoint());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('DEL', $this->_object->getName());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiOrgUnicodeName('007F', 'DEL');
	}
	
}
